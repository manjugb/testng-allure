package com.mystore.selenium.testing.config;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.aeonbits.owner.ConfigCache;

public class ConfigurationManager {

    private ConfigurationManager() {
    }

    public static Configuration configuration() {
        return ConfigCache.getOrCreate(Configuration.class);
    }
    
    /*public static void generateAllureReport() {
        String pattern = "dd-MM-yyyy_HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String reportfolder = "allure-report_" + simpleDateFormat.format(new Date());
        executeShellCmd("allure generate allure-results");
        executeShellCmd("mv allure-report " + reportfolder);
        executeShellCmd("cp -R src/main/resources/config/allure-2.13.5 "+reportfolder);
       // executeShellCmd("cp src/main/resources/config/open_report_mac.sh "+reportfolder);
        executeShellCmd("cp src/main/resources/config/open_report_windows.bat "+reportfolder);
    }

    public static void executeShellCmd(String shellCmd) {
        try {
            Process process = Runtime.getRuntime().exec(shellCmd);
            process.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error in Executing the command " + shellCmd);
        }
    }*/
}

