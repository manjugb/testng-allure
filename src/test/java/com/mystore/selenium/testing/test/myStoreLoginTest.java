package com.mystore.selenium.testing.test;

import org.testng.annotations.Test;

import com.mystore.selenium.testing.data.MyStoreLoginDataFactory;
import com.mystore.selenium.testing.data.excelreader;
import com.mystore.selenium.testing.model.MyStoreLogin;
import com.mystore.selenium.testing.page.Login.LoginPage;

public class myStoreLoginTest extends BaseWeb {
	excelreader reader = new excelreader("./data/logindata.xlsx");
	String error_email = reader.getCellData("Login", "ErrorMessage", 2);
	String error_psw = reader.getCellData("Login", "ErrorMessage", 3);
	
		
    @Test(description = "MyStorLogin Test using Fake Data")
	public void myStoreLogin_Invalid_Credentials() {
		MyStoreLogin loginformation = new MyStoreLoginDataFactory().createLoginData();
		LoginPage loginPage = new LoginPage();
		loginPage.login();
		loginPage.fillEmail(loginformation.getEmail());
		loginPage.fillPassword(loginformation.getPassword());
		loginPage.submit();
		loginPage.verifyErrorMsg(error_email);
		//loginPage.verifyUsername(loginformation.getUser());
		

	}
    
    
    @Test(description = "MyStorLogin Test using Fake Data")
	public void myStoreLogin_with_password() {
		MyStoreLogin loginformation = new MyStoreLoginDataFactory().createLoginData();
		LoginPage loginPage = new LoginPage();
		loginPage.login();
		loginPage.fillPassword(loginformation.getPassword());
		loginPage.submit();
		loginPage.verifyErrorMsg(error_email);
		//loginPage.verifyUsername(loginformation.getUser());
		

	}
    
    
    @Test(description = "MyStorLogin Test using Fake Data")
   	public void myStoreLogin_with_email() {
   		MyStoreLogin loginformation = new MyStoreLoginDataFactory().createLoginData();
   		LoginPage loginPage = new LoginPage();
   		loginPage.login();
   		loginPage.fillEmail(loginformation.getEmail());
   		loginPage.submit();
   		loginPage.verifyErrorMsg(error_psw);
   		//loginPage.verifyUsername(loginformation.getUser());
   		

   	}
    
    
    @Test(description = "MyStorLogin Test using Fake Data")
   	public void myStoreLogin_with_empty() {
   		MyStoreLogin loginformation = new MyStoreLoginDataFactory().createLoginData();
   		LoginPage loginPage = new LoginPage();
   		loginPage.login();
   		loginPage.fillEmail("");
   		loginPage.fillPassword("");
   		
   		loginPage.submit();
   		loginPage.verifyErrorMsg(error_email);
   		//loginPage.verifyUsername(loginformation.getUser());
   		

   	}


	
}
