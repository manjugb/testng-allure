package com.mystore.selenium.testing.test;

import static com.mystore.selenium.testing.config.ConfigurationManager.configuration;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.mystore.selenium.testing.driver.DriverManager;
import com.mystore.selenium.testing.driver.TargetFactory;
import com.mystore.selenium.testing.report.AllureManager;



@Listeners({TestListener.class})
public abstract class BaseWeb {
    public static WebDriver driver;
    @BeforeSuite
    public void beforeSuite() {
        AllureManager.setAllureEnvironmentInformation();
    }

    @BeforeMethod(alwaysRun = true)
    @Parameters("browser")
    public void preCondition(@Optional("chrome") String browser) {
        driver = new TargetFactory().createInstance(browser);
        DriverManager.setDriver(driver);

        DriverManager.getDriver().get(configuration().url());
    }
    
   /* @Given("^I Go to MyStore Page in \"([^\"]*)\"$")
    public void go_to_url(String browser) {
    	preCondition(browser);
    }*/

    @AfterMethod(alwaysRun = true)
    public void postCondition() {
        DriverManager.quit();
    }
}
