package com.mystore.selenium.testing.test;

import java.io.IOException;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mystore.selenium.testing.data.excelreader;
import com.mystore.selenium.testing.page.Login.LoginPage;


public class myStoreLoginDataTest extends BaseWeb{

	//.InputStream readFile = getClass().getResourceAsStream("/logindata.xlsx");
	
	excelreader reader = new excelreader("./data/logindata.xlsx");
	
	String userName = reader.getCellData("Login", "UserName", 2);
	String password = reader.getCellData("Login", "Password", 2);
	String logeduser = reader.getCellData("Login", "LogedUser", 2);
	
	    @Test(description="MyStoreLogin Test Using XL")
	    @Parameters(value="logindata")
	    public void myStoreLogin_readcsv() throws IOException {
	        //MyStoreLogin loginformation = new MyStoreLoginDataFactory().createLoginData();

	        LoginPage loginPage = new LoginPage();
	        loginPage.login();
	        loginPage.fillEmail(userName);
	        loginPage.fillPassword(password);
	        loginPage.submit();
	        loginPage.verifyUsername(logeduser);

	    }
	
	
}
