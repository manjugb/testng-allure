package com.mystore.selenium.testing.data;

import static com.mystore.selenium.testing.config.ConfigurationManager.configuration;

import java.util.Locale;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.javafaker.Faker;
import com.mystore.selenium.testing.model.MyStoreLogin;
import com.mystore.selenium.testing.model.MyStoreLoginBuilder;


public class MyStoreLoginDataFactory {
	private final Faker faker;
    private static final Logger logger = LogManager.getLogger(MyStoreLoginDataFactory.class);

    public MyStoreLoginDataFactory() {
        faker = new Faker(new Locale(configuration().faker()));
    }

    public MyStoreLogin createLoginData() {
    	MyStoreLogin mystore = new MyStoreLoginBuilder().
            email(faker.internet().emailAddress()).
            password(faker.internet().password()).
            logeduser(faker.internet().avatar()).
            build();

        logger.info(mystore);
        return mystore;
    }

  

}
