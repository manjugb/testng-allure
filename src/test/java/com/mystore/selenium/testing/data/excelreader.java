package com.mystore.selenium.testing.data;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFCreationHelper;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class excelreader {

	
	public String path;
	public FileInputStream fis = null;
	private XSSFWorkbook workbook = null;
	private XSSFSheet sheet = null;
	private XSSFRow row = null;
	private XSSFCell cell = null;
	
	public excelreader(String path) {
		this.path = path;
		try {
			fis = new FileInputStream(path);
			workbook = new XSSFWorkbook(fis);
			sheet = workbook.getSheetAt(0);
			fis.close();
		}catch (Exception e) {
			e.printStackTrace();
			
		}
	}
	
	public String getCellData(String sheetName, String colName, int rowNum) {
		 int index = workbook.getSheetIndex(sheetName);
		 int col_Num =0;
		 sheet = workbook.getSheetAt(index);
		 
		 row =sheet.getRow(0);
		 for (int i = 0; i < row.getLastCellNum(); i++) {
			 if(row.getCell(i).getStringCellValue().trim().equals(colName.trim())) {
				 col_Num = i;
				 
			 }
		 }
	
		row = sheet.getRow(rowNum -1);
		 cell= row.getCell(col_Num);
		return cell.getStringCellValue();
		
		
	}
		
		
		
		
	}