package com.mystore.selenium.testing.page.Login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.mystore.selenium.testing.page.booking.common.MyStoreNavigationPage;

import io.qameta.allure.Step;

public class LoginPage extends MyStoreNavigationPage{
	
	
	@FindBy(how = How.XPATH,using="//input[@id='email']")
	private WebElement email;
	
	@FindBy(how=How.XPATH,using="//input[@id='passwd']")
	private WebElement password;
	
	
	@Step
    public void fillEmail(String email) {
        this.email.sendKeys(email);
    }

    @Step
    public void fillPassword(String password) {
        this.password.sendKeys(password);
    }
    
    

	
}
