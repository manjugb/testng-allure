package com.mystore.selenium.testing.page.booking.common;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import com.mystore.selenium.testing.page.AbstractPageObject;

import io.qameta.allure.Step;

public class MyStoreNavigationPage extends AbstractPageObject{
	
	
	
	@FindBy(how=How.XPATH,using="//button[@id='SubmitLogin']")
	private WebElement submit;
	
	@FindBy(how=How.PARTIAL_LINK_TEXT,using="Sign")
	private WebElement login;
	
	@FindBy(how=How.XPATH,using="/html[1]/body[1]/div[1]/div[1]/header[1]/div[2]/div[1]/div[1]/nav[1]/div[1]/a[1]/span[1]")
	private WebElement username;
	
	@FindBy(how=How.XPATH,using="/html[1]/body[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/ol[1]/li[1]")
	private WebElement errormsg;
	
	
	@Step
	public void login() {
		System.out.println("Click Started");
		login.click();
		System.out.println("CLick ended");
    }
	
	@Step
	public void submit() {
		submit.click();
    }

	@Step
    public void verifyUsername(String logeduser) {
    	String actuser = this.username.getText();
    	if(actuser.equalsIgnoreCase(logeduser)) {
    	  Assert.assertEquals(actuser, logeduser);
    	}else {
    		Assert.assertNotEquals(actuser, logeduser);
    	}
    }
	
	@Step
    public void verifyErrorMsg(String error) {
    	String actuser = errormsg.getText();
    	if(actuser.equalsIgnoreCase(error)) {
    	  Assert.assertEquals(actuser, error);
    	}else {
    		Assert.assertNotEquals(actuser, error);
    	}
    }

}
