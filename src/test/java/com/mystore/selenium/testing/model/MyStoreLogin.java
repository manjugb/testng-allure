package com.mystore.selenium.testing.model;

import com.mystore.selenium.testing.enums.RoomType;

public class MyStoreLogin {
	    private String email;
	    private String password;
	    private String logeduser;
	    
	   
	    public MyStoreLogin(String email,String password,String user) {
	        this.email = email;
	        this.password = password;
	        this.logeduser = user;
	        
	        
	        
	    }

	    public MyStoreLogin() {
	    }

	    public String getEmail() {
	        return this.email;
	    }

	   
	    public String getPassword() {
	        return this.password;
	    }
	    
	    public String getUser() {
	    	return this.logeduser;
	    }

	    


	    public void setEmail(String email) {
	        this.email = email;
	    }

	   

	    public void setPassword(String password) {
	        this.password = password;
	    }
	    
	    public void setUser(String user)   {
	    	this.logeduser=user;
	    }
	    

	    
	    public String toString() {
	        return "MyStoreLogin(email=" + this.getEmail() + ",user="+this.getUser()+")";
	    }

	}


