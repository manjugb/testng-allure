package com.mystore.selenium.testing.model;

import com.mystore.selenium.testing.enums.RoomType;

public class MyStoreLoginBuilder {

    private String email;
    private String password;
    private String logeduser;


    public MyStoreLoginBuilder email(String email) {
        this.email = email;
        return this;
    }

    
    public MyStoreLoginBuilder password(String password) {
        this.password = password;
        return this;
    }
    
    public MyStoreLoginBuilder logeduser(String password) {
        this.logeduser = password;
        return this;
    }

    
    public MyStoreLogin build() {
        return new MyStoreLogin(email,password,logeduser);
    }
}